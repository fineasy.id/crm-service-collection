package id.fineasy.crm.collection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.annotation.EnableJms;

@EnableJms
@EnableEurekaClient
@SpringBootApplication(scanBasePackages = {"id.fineasy.crm.collection"})
public class Application {
    public static void main(String[] args) {
        System.setProperty("server.servlet.context-path", "/api/collection");
        ApplicationContext context =  SpringApplication.run(Application.class, args);

        Application app = context.getBean(Application.class);
        app.injectContext(context);
    }

    private void injectContext(ApplicationContext context) {

    }
}
