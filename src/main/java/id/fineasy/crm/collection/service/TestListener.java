package id.fineasy.crm.collection.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import javax.jms.Session;
import java.lang.invoke.MethodHandles;

@Component
public class TestListener {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @JmsListener(destination = "test-only-please")
    public void testListener(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        log.info("Received message: "+jsonObjectString);
    }
}
